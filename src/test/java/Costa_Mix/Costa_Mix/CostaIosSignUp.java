package Costa_Mix.Costa_Mix;



import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;

public class CostaIosSignUp {
	
	public AppiumDriver driver;
	public WebDriverWait wait;
	
	
	@Test
	public void CostaAppHome() throws InterruptedException, IOException {
	
	

	 final File appDir = new File("/Users/doruk/Library/Developer/Xcode/DerivedData/CostaLoyalty-gggjjtzvrdmqkuamksjxdcmofhmw/Build/Products/Debug-iphonesimulator");

	 final File app = new File(appDir, "CostaLoyalty.app"); // give the path of .app file/. ipa file .

	 DesiredCapabilities caps = new DesiredCapabilities();
	 
	   
	    
	 caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 11");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, 13.3);
		caps.setCapability(MobileCapabilityType.NO_RESET, true);
		caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
		caps.setCapability("useNewWDA", false);
		 caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
		
		 caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		 
		 caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("autoAcceptAlerts", true);
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			
			
		

		

		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");

		IOSDriver<MobileElement> driver = new IOSDriver<MobileElement>(url, caps);
		
		
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"kuwait flag\"]"), driver).click();

		
		Thread.sleep(2000);
		
        getElement(By.xpath("//XCUIElementTypeButton[@name=\"English\"]"), driver).click();

		
		Thread.sleep(2000);
		
		 getElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]"), driver).click();

			
     	Thread.sleep(2000);
     	
     	 
      	
      	
		
		
		
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"), driver).click();
		Thread.sleep(1000);
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"), driver).clear();
		Thread.sleep(1000);
	
		driver.switchTo().activeElement().sendKeys("90000907");
		Thread.sleep(2000);
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Send SMS Code\"]"),driver).click();
		Thread.sleep(2000);

		driver.switchTo().activeElement().sendKeys("1111");
		Thread.sleep(2000);

		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]"),driver).click();
		Thread.sleep(2000);
		
		
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"icon crossWhite\"]"),driver).click();
		Thread.sleep(2000);
		

		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField[1]"),driver).click();
		Thread.sleep(2000);
		
		driver.switchTo().activeElement().sendKeys("Doruk");
		
		Thread.sleep(2000);
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField[2]"),driver).click();
		Thread.sleep(2000);
		
		driver.switchTo().activeElement().sendKeys("Taskin");
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeTextField[1]"),driver).click();
		Thread.sleep(2000);
		
		driver.switchTo().activeElement().sendKeys("doruk.taskin@appcent.mobi");
		Thread.sleep(2000);
		
		
	
		
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeButton[1]"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeButton[1]"),driver).click();
		Thread.sleep(2000);
		
		
        getElement(By.xpath("//XCUIElementTypeButton[@name=\"Sign up\"]"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Confirm\"]"),driver).click();
		Thread.sleep(2000);
		
		
		
		
		
		
		
		
		

		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[4]"),driver).click();
		Thread.sleep(2000);
		
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton"),driver).click();
		Thread.sleep(2000);
		
		
		

		/*
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[3]"
		 * ),driver).click(); Thread.sleep(2000);
		 */
		
		getElement(By.id("icon arrowLeftWhite"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.id("icon crossWhite"),driver).click();
		Thread.sleep(2000);
		
		
		
		
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"PROFILE\"]"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.id("icon settings"),driver).click();
		Thread.sleep(2000);
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[9]"),driver).click();
		
		
		

	 

		/*
		 * caps.setCapability("appPackage", "package of your application"); //package of
		 * the application
		 * 
		 * caps.setCapability("appActivity", "activity of your application"); //app
		 * activity of the application
		 */	 

}
	
	public MobileElement getElement(By byelement, IOSDriver<MobileElement> driver2) {

		WebDriverWait wait = new WebDriverWait(driver2, 58, 2000);
		MobileElement element = (MobileElement) wait.until(ExpectedConditions.presenceOfElementLocated(byelement));
		return element;

	}
	
	private static void scroll(AppiumDriver<?> driver, int fromX, int fromY, int toX, int toY) {
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();

	}
	
	private BufferedImage generateImage( MobileElement element, File screenshot) throws IOException {
	    BufferedImage fullImage = ImageIO.read(screenshot);
	    Point imageLocation = element.getLocation();

	    int qrCodeImageWidth = element.getSize().getWidth();
	    int qrCodeImageHeight = element.getSize().getHeight();

	    int pointXPosition = imageLocation.getX();
	    int pointYPosition = imageLocation.getY();

	    BufferedImage qrCodeImage = fullImage.getSubimage(pointXPosition, pointYPosition, qrCodeImageWidth, qrCodeImageHeight);
	    ImageIO.write(qrCodeImage, "png", screenshot);

	    return qrCodeImage;
	}

	public static void scroll(AppiumDriver<?> driver, By by) {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		boolean isFoundTheElement = driver.findElements(by).size() > 0;
		while (isFoundTheElement == false) {
			scrollDown(driver);
			isFoundTheElement = driver.findElements(by).size() > 0;
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		
	}

	public static void scrollDown(AppiumDriver<?> driver) {
		// if pressX was zero it didn't work for me
		int pressX = driver.manage().window().getSize().width * 15 / 16;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver.manage().window().getSize().height * 2 / 3;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver.manage().window().getSize().height / 8;
		// scroll with TouchAction by itself
		scroll(driver, pressX, bottomY, pressX, topY);
	}
	
	public static void scrollUp(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, topY, pressX, bottomY);
	}
	
	 void assertText(By element, String text) throws InterruptedException {
	        wait.until(ExpectedConditions.presenceOfElementLocated(element));
	        Assert.assertEquals(driver.findElement(element).getText(),"");
	    }
	

}

