package Costa_Mix.Costa_Mix;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.touch.offset.PointOption;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



import javax.imageio.ImageIO;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNotSame;
import static org.testng.Assert.assertSame;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Costa_Language_Arabic {
	
	@Test
	public void CostaApp() throws InterruptedException, IOException, NotFoundException {
	
	

	final File appDir = new File("/Users/doruk/Library/Developer/Xcode/DerivedData/CostaLoyalty-dphcijbastztodbcxucciuhgoyxb/Build/Products/Debug-iphonesimulator");

	 final File app = new File(appDir, "CostaLoyalty.app"); // give the path of .app file/. ipa file .

	 DesiredCapabilities caps = new DesiredCapabilities();
	 
	   
	    
	 caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 11");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, 13.3);
		caps.setCapability(MobileCapabilityType.NO_RESET, true);
		caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");

		caps.setCapability("useNewWDA", false);
		 caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
		
		 caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		 
		 caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("autoAcceptAlerts", true);
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("sensorInstrument", true);

		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");

		IOSDriver<MobileElement> driver = new IOSDriver<MobileElement>(url, caps);
		
		
		
		/*
		 * Logging in and navigate to Settings to change the language to Arabic
		 */		
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"), driver).click();
		Thread.sleep(1000);
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"), driver).clear();
		Thread.sleep(1000);
	
		driver.switchTo().activeElement().sendKeys("90000907");
		Thread.sleep(2000);
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Send SMS Code\"]"),driver).click();
		Thread.sleep(2000);

		driver.switchTo().activeElement().sendKeys("1111");
		Thread.sleep(2000);

		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"PROFILE\"]"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.id("icon settings"),driver).click();
		Thread.sleep(2000);
		
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]"),driver).click();
		Thread.sleep(2000);
	
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"),driver).sendKeys("Arabic");
		Thread.sleep(2000);

		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Done\"]"),driver).click();
		Thread.sleep(2000);
		
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"المسح\"]"), driver).click();
		
		Thread.sleep(2000);
		
		
		
		/*
		 * Checking first qr code and split on the A and get the profilenumber and random qr number
		 */
		
		
		  String folder_namearab = "screenshootarab"; SimpleDateFormat dfqrarab = new
		  SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa"); new File(folder_namearab).mkdir();
		String file_namearab = dfqrarab.format(new Date())
				+ ".png";
		
		/*
		 * File fqr2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 * FileUtils.copyFile(fqr2, new File(folder_name2 + "/" + file_name2));
		 */
		 	
		  MobileElement elementarab = driver.findElement(By.
		  xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeImage[1]"
		  )); File fqrarab =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  Point pointarab = elementarab.getLocation(); 
		  int xcordinatearab = pointarab.getX(); 
		  int ycordinatearab = pointarab.getY(); 
		  int imageWidtharab = elementarab.getSize().getWidth();
		  //Retrieve height of element. 
		  int imageHeightarab =
		  elementarab.getSize().getHeight();
		  
		  BufferedImage imgqrarab = ImageIO.read(fqrarab);
		  
		  //cut Image using height, width and x y coordinates parameters. 
		  
		  BufferedImage destinationqrqrarab = imgqrarab.getSubimage(180, 490, 590, 590);
		  ImageIO.write(destinationqrqrarab, "png", fqrarab);
		  
		  //save Image screenshot In D: drive.
		  
		  FileUtils.copyFile(fqrarab, new File(folder_namearab + "/" + file_namearab));
		 
		  String qrCodenoarab = decodeQRCode(destinationqrqrarab);
		  System.out.println("QrCode is succesfully scanned: "+qrCodenoarab);
		   String qrCodesarab= qrCodenoarab ;
	       String[] arrOfStrarab = qrCodesarab.split("A", 5); 
	 
	        for (String a : arrOfStrarab) {
	        	if(a.length()>0) {
	        		
	        		System.out.println("QrCode is correct and is "+a+" and has the following number of characters: "+a.length());
	        	}
	        }
	        
	       assertNotNull(qrCodenoarab);
	       
	       
	      long qrProfileNumberarab = 9990000000008228L;
	       String qrControlearab=arrOfStrarab[2];
	       
	       Assert.assertEquals(qrControlearab,String.valueOf(qrProfileNumberarab));
	       
	       
	       
	       
	       
		
	       
	       getElement(By.xpath("//XCUIElementTypeButton[@name=\"الصفحة الرئيسية\"]"),driver).click();
			Thread.sleep(2000);
			
			getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[1]"),driver).click();
			Thread.sleep(5000);
			
			getElement(By.xpath("//XCUIElementTypeButton[@name=\"icon arrowLeftWhite\"]"),driver).click();
			
			Thread.sleep(2000);
			
			getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[5]"),driver).click();
			
			Thread.sleep(2000);
			
			getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton"),driver).click();
			Thread.sleep(2000);
			

		getElement(By.id("icon arrowLeftWhite"),driver).click();
		Thread.sleep(3000);
		getElement(By.id("icon crossWhite"),driver).click();
		
		Thread.sleep(2000);
		
scrollDown(driver);
Thread.sleep(2000);


scrollUp(driver);
Thread.sleep(2000);
scrollUp(driver);
Thread.sleep(2000);


getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[2]"),driver).click();
Thread.sleep(4000);

getElement(By.xpath("//XCUIElementTypeStaticText[@name=\"إستبدل البينز\"]"),driver).click();

Thread.sleep(4000);
getElement(By.id("icon crossBlack"),driver).click();

Thread.sleep(2000);
getElement(By.xpath("//XCUIElementTypeButton[@name=\"المسح\"]"), driver).click();
Thread.sleep(2000);

		/*
		 * Checking qr code refresh after 1 minute passed
		 */


String folder_namearab2 = "screenshottaarab"; SimpleDateFormat
dfqrarab2 = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
new File(folder_namearab2).mkdir();
String file_namearab2 = dfqrarab2.format(new Date()) + ".png";



 File fqrarab2 =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

 
 BufferedImage imgqrarab2 = ImageIO.read(fqrarab2);
 
 //cut Image using height, width and x y coordinates parameters. 
 
 BufferedImage destinationqrqrarab2 = imgqrarab2.getSubimage(180, 490, 590, 590);
 ImageIO.write(destinationqrqrarab2, "png", fqrarab2);
 
 //save Image screenshot In D: drive.
 
 
 
 String qrCode1noarab = decodeQRCode(destinationqrqrarab2);
 System.out.println();
 System.out.println("QrCode is succesfully scanned: "+qrCode1noarab);

 if(qrCodenoarab.equals(qrCode1noarab)) { 
	  System.out.println("qrCode has not refreshed");
	  
 }
 
 else {
	  System.out.println("qrCode has succesfully refreshed, new qrCode is not the same as the old one. ");
	  
 }
 
 
 assertNotSame(qrCodenoarab,qrCode1noarab);
 
 
		/*
		 * Going to settings and change language to english
		 */ 
 
 
 Thread.sleep(2000);
 getElement(By.xpath("//XCUIElementTypeButton[@name=\"الصفحة الرئيسية\"]"),driver).click();
 Thread.sleep(2000);
 
 scrollDown(driver);
 Thread.sleep(2000);

 scrollDown(driver);
 Thread.sleep(2000);
 

 getElement(By.xpath("//XCUIElementTypeButton[@name=\"إستخدم\"]"),driver).click();

 Thread.sleep(2000);
 
 
 
 
 String folder_namearab6 = "screenshooootarab6"; SimpleDateFormat dfqrarab6 = new
		  SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa"); new File(folder_namearab6).mkdir();
		String file_namearab6 = dfqrarab6.format(new Date())
				+ ".png";

File farab6 =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	
 
 BufferedImage imgqrarab6 = ImageIO.read(farab6);
 
 //cut Image using height, width and x y coordinates parameters. 
 
 BufferedImage destinationqrqrarab6 = imgqrarab6.getSubimage(180, 490, 590, 590);
 ImageIO.write(destinationqrqrarab6, "png", farab6);
 
 //save Image screenshot In D: drive.
 
 
 
 String qrCode2noarab6 = decodeQRCode(destinationqrqrarab6);
 
 System.out.println("Couponcode is succesfully scanned: "+qrCode2noarab6);


 String qrCodeUsearab6= qrCode2noarab6 ;
   String[] arrOfStrUsearab6 = qrCodeUsearab6.split("A", 5); 

   for (String b : arrOfStrUsearab6) {
   	System.out.println(b);
   	
   	if(b.length()>0) {
   		
   		
   		System.out.println("QrCode is correct and is "+b+" and has the following number of characters: "+b.length());
   	}
   
   }
 
   
   assertNotSame(qrCodenoarab,qrCode2noarab6);
   assertNotSame(qrCode1noarab,qrCode2noarab6);
 
 

 
 
 getElement(By.xpath("//XCUIElementTypeButton[@name=\"البيانات\"]"),driver).click();
 Thread.sleep(2000);
 getElement(By.id("icon settings"),driver).click();
 Thread.sleep(2000);

 
 getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]"),driver).click();

 Thread.sleep(2000);
 
	getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"),driver).sendKeys("English");
	Thread.sleep(2000);

	getElement(By.xpath("//XCUIElementTypeButton[@name=\"إنهاء\"]"),driver).click();
	Thread.sleep(2000);
 
	

 
	
	// Sign out while you are at settings already.
		/*
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[9]"
		 * ),driver).click();
		 * 
		 * Thread.sleep(2000);
		 * 
		 * 
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"تسجيل الخروج\"]"),driver
		 * ).click();
		 */


}
	

	
	
	
	public MobileElement getElement(By byelement, IOSDriver<MobileElement> driver2) {

		WebDriverWait wait = new WebDriverWait(driver2, 58, 2000);
		MobileElement element = (MobileElement) wait.until(ExpectedConditions.presenceOfElementLocated(byelement));
		return element;
}

	public MobileElement getElement2 (By byelement, IOSDriver<MobileElement>driver3) {
         
       MobileElement element=driver3.findElement(By.xpath("//XCUIElementTypeButton[@name=\\\"icon arrowLeftWhite\\\"]"));
       TouchActions actions=new TouchActions(driver3);
       actions.singleTap(element).perform();          
      return element;  
	}
  
  
  
	private static void scroll(AppiumDriver<?> driver, int fromX, int fromY, int toX, int toY) {
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();
	}
	

	public static void scroll(AppiumDriver<?> driver, By by) {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		boolean isFoundTheElement = driver.findElements(by).size() > 0;
		while (isFoundTheElement == false) {
			scrollDown(driver);
			isFoundTheElement = driver.findElements(by).size() > 0;
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		
	}

	public static void scrollDown(AppiumDriver<?> driver) {
		// if pressX was zero it didn't work for me
		int pressX = driver.manage().window().getSize().width * 15 / 16;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver.manage().window().getSize().height * 2 / 3;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver.manage().window().getSize().height / 8;
		// scroll with TouchAction by itself
		scroll(driver, pressX, bottomY, pressX, topY);
	}
	
	public static void scrollUp(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, topY, pressX, bottomY);
	}
	
	
	
	public static void scrollDown2(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, bottomY, pressX, topY);
	
	}
	
	
	


    private AppiumDriver<MobileElement> driver;
    private AppiumDriverLocalService service;

  

    /**
     * This test capture the screenshot and get the element that contains the QRCode
     * Based on the element points (width and height) the image os cropped
     * With cropped image we can decode the QRCode with zxing
     * @throws InterruptedException 
     */
   

    /**
     * Return a cropped image based on an element (in this case the qrcode image) from the entire device screenshot
     * @param element elemement that will show in the screenshot
     * @param screenshot the entire device screenshot
     * @return a new image in BufferedImage object
     * @throws IOException if any problem in generate image occurs
     */
    private static BufferedImage generateImage( MobileElement element, File screenshot) throws IOException {
        BufferedImage fullImage = ImageIO.read(screenshot);
        Point imageLocation = element.getLocation();

        int qrCodeImageWidth = element.getSize().getWidth();
        int qrCodeImageHeight = element.getSize().getHeight();

        int pointXPosition = imageLocation.getX();
        int pointYPosition = imageLocation.getY();

        BufferedImage qrCodeImage = fullImage.getSubimage(pointXPosition, pointYPosition, qrCodeImageWidth, qrCodeImageHeight);
        ImageIO.write(qrCodeImage, "png", screenshot);

        return qrCodeImage;
    }

    /**
     * Decode a QR Code image using zxing
     * @param qrCodeImage the qrcode image cropped from entire device screenshot
     * @return the content
     * @throws NotFoundException if the image was not found
     */
    private static String decodeQRCode(BufferedImage qrCodeImage) throws NotFoundException {
        LuminanceSource source = new BufferedImageLuminanceSource(qrCodeImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Result result = new MultiFormatReader().decode(bitmap);
        return result.getText();
    }
    
  

}