package Costa_Mix.Costa_Mix;

import static org.testng.Assert.assertNotNull;

import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidKeyMetastate;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.offset.PointOption;

public class CostaAndroid_Wiggle {
	
	@Test
	public void CostaApp() throws InterruptedException, IOException, NotFoundException {
	
	
	

	DesiredCapabilities bc = new DesiredCapabilities();

	bc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
	bc.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
	bc.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus_4_API_28");
	bc.setCapability(MobileCapabilityType.PLATFORM_VERSION, 9.0);
	bc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
		
		
		  bc.setCapability("appPackage", "com.alghanim.costakuwait");
		  bc.setCapability("appActivity",
		  "mobi.appcent.costaloyalty.activity.splash.SplashActivity");
		 
		 
	
		
		  
	
	bc.setCapability("autoGrantPermissions", true);
	bc.setCapability("autoAcceptAlerts", true);
	bc.setCapability("unicodeKeyboard", true);
	bc.setCapability("resetKeyboard", true);
	
	
	

	// Declare server

	URL url = new URL("http://127.0.0.1:4723/wd/hub");

	AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(url, bc);
	
	getElement(By.id("tvSkip"), driver).click();

	Thread.sleep(2000);
	
	getElement(By.id("etPhone"), driver).click();
	Thread.sleep(2000);
	
	
	getElement(By.id("etPhone"), driver).clear();
	Thread.sleep(3000);

	
	
	 
		
		  KeyInput keyboard = new KeyInput("keyboard"); 
		  Sequence sendKeys = new Sequence(keyboard, 0);
		  
		  sendKeys.addAction(keyboard.createKeyDown("9".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("9".codePointAt(0)));
		  
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  
		  sendKeys.addAction(keyboard.createKeyDown("9".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("9".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("7".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("7".codePointAt(0)));
		  
		  
		  
		  driver.perform(Arrays.asList(sendKeys));
		  
		  Thread.sleep(4000);
		 
	   
	   
	 
	   scrollDown(driver);
	   
	   
	 
		
		/*
		 * if (getElement(By.id("btnSendSms"),driver2).isDisplayed()) {
		 * 
		 * getElement(By.id("btnSendSms"), driver2).click();
		 * 
		 * } else if (getElement(By.id("etPinCode"),driver2).isDisplayed()) {
		 * 
		 * getElement(By.id("etPinCode"), driver2).sendKeys("1111");
		 * 
		 * 
		 * }
		 */
		
		   
	   Thread.sleep(4000);

	 
		getElement(By.id("btnSendSms"), driver).click();
		
		
		
		  
		  Thread.sleep(4000);
		
		getElement(By.id("etPinCode"), driver).sendKeys("1111");
		  Thread.sleep(4000);

		
		  scrollDown(driver);
		
		 Thread.sleep(2000);

		getElement(By.id("btnContinue"), driver).click();
		
		Thread.sleep(3000);
		
		scrollDown(driver);
		Thread.sleep(3000);
		scrollDown(driver);
		Thread.sleep(3000);
		scrollDown(driver);

		

		getElement(By.id("com.alghanim.costakuwait:id/ivSecretOffers"), driver).click();
		Thread.sleep(3000);
		
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_F2);

	}

	
	public MobileElement getElement(By byelement, AndroidDriver<MobileElement> driver) {

		WebDriverWait wait = new WebDriverWait(driver, 58, 2000);
		MobileElement element = (MobileElement) wait.until(ExpectedConditions.presenceOfElementLocated(byelement));
		return element;

	}
	
	 

  
  
  
	private static void scroll(AppiumDriver<?> driver, int fromX, int fromY, int toX, int toY) {
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();

	}
	

	public static void scroll(AppiumDriver<?> driver, By by) {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		boolean isFoundTheElement = driver.findElements(by).size() > 0;
		while (isFoundTheElement == false) {
			scrollDown(driver);
			isFoundTheElement = driver.findElements(by).size() > 0;
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		
	}

	public static void scrollDown(AppiumDriver<?> driver) {
		// if pressX was zero it didn't work for me
		int pressX = driver.manage().window().getSize().width * 15 / 16;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver.manage().window().getSize().height * 2 / 3;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver.manage().window().getSize().height / 8;
		// scroll with TouchAction by itself
		scroll(driver, pressX, bottomY, pressX, topY);
	}
	
	public static void scrollUp(AppiumDriver<?> driver) {
		// if pressX was zero it didn't work for me
				int pressX = driver.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver, pressX, topY, pressX, bottomY);
	}
	
	
	
	public static void scrollDown2(AppiumDriver<?> driver) {
		// if pressX was zero it didn't work for me
				int pressX = driver.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver, pressX, bottomY, pressX, topY);
	}
	
	
	


    private AppiumDriver<MobileElement> driver;
    private AppiumDriverLocalService service;

  

    /**
     * This test capture the screenshot and get the element that contains the QRCode
     * Based on the element points (width and height) the image os cropped
     * With cropped image we can decode the QRCode with zxing
     * @throws InterruptedException 
     */
   

    /**
     * Return a cropped image based on an element (in this case the qrcode image) from the entire device screenshot
     * @param element elemement that will show in the screenshot
     * @param screenshot the entire device screenshot
     * @return a new image in BufferedImage object
     * @throws IOException if any problem in generate image occurs
     */
    private static BufferedImage generateImage( MobileElement element, File screenshot) throws IOException {
        BufferedImage fullImage = ImageIO.read(screenshot);
        Point imageLocation = element.getLocation();

        int qrCodeImageWidth = element.getSize().getWidth();
        int qrCodeImageHeight = element.getSize().getHeight();

        int pointXPosition = imageLocation.getX();
        int pointYPosition = imageLocation.getY();

        BufferedImage qrCodeImage = fullImage.getSubimage(pointXPosition, pointYPosition, qrCodeImageWidth, qrCodeImageHeight);
        ImageIO.write(qrCodeImage, "png", screenshot);

        return qrCodeImage;
    }

    /**
     * Decode a QR Code image using zxing
     * @param qrCodeImage the qrcode image cropped from entire device screenshot
     * @return the content
     * @throws NotFoundException if the image was not found
     */
    private static String decodeQRCode(BufferedImage qrCodeImage) throws NotFoundException {
        LuminanceSource source = new BufferedImageLuminanceSource(qrCodeImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Result result = new MultiFormatReader().decode(bitmap);
        return result.getText();
    }
    
  
	
	
	
	
	
}
	
