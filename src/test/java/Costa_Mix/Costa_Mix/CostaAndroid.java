package Costa_Mix.Costa_Mix;



import org.testng.annotations.Test;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.DeviceRotation;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableMap;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.touch.offset.PointOption;

public class CostaAndroid {
	
	@Test
	public void CostaApp() throws MalformedURLException, InterruptedException {
	
	
	

	DesiredCapabilities bc = new DesiredCapabilities();

	bc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
	bc.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
	bc.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus_4_API_28");
	bc.setCapability(MobileCapabilityType.PLATFORM_VERSION, 9.0);
	bc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
		
		
		  bc.setCapability("appPackage", "com.alghanim.costakuwait");
		  bc.setCapability("appActivity",
		  "mobi.appcent.costaloyalty.activity.splash.SplashActivity");
		 
		 
	
		
		  
	
	bc.setCapability("autoGrantPermissions", true);
	bc.setCapability("autoAcceptAlerts", true);
	bc.setCapability("unicodeKeyboard", true);
	bc.setCapability("resetKeyboard", true);
	
	
	

	// Declare server

	URL url = new URL("http://127.0.0.1:4723/wd/hub");

	AndroidDriver<MobileElement> driver2 = new AndroidDriver<MobileElement>(url, bc);
	
	

	
	getElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageButton[1]"), driver2).click();

	Thread.sleep(2000);
	getElement(By.id("btEnglishLanguage"), driver2).click();
	Thread.sleep(3000);
	getElement(By.id("btContinue"), driver2).click();
	
	
	Thread.sleep(3000);


	
	
	
	getElement(By.id("tvSkip"), driver2).click();
	
	
	Thread.sleep(3000);
	
	scrollDown(driver2);
	
	Thread.sleep(2000);
	
	getElement(By.id("etPhone"), driver2).click();
	Thread.sleep(2000);
	
	
	getElement(By.id("etPhone"), driver2).clear();
	Thread.sleep(3000);

	
	
	 
		
		  KeyInput keyboard = new KeyInput("keyboard"); 
		  Sequence sendKeys = new Sequence(keyboard, 0);
		  
		  sendKeys.addAction(keyboard.createKeyDown("9".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("9".codePointAt(0)));
		  
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  
		  sendKeys.addAction(keyboard.createKeyDown("9".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("9".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("0".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyDown("7".codePointAt(0)));
		  sendKeys.addAction(keyboard.createKeyUp("7".codePointAt(0)));
		  
		  
		  
		  driver2.perform(Arrays.asList(sendKeys));
		  
		  Thread.sleep(4000);
		 
	   
	   
	 
	   scrollDown(driver2);
	   
	   
	 
		
		/*
		 * if (getElement(By.id("btnSendSms"),driver2).isDisplayed()) {
		 * 
		 * getElement(By.id("btnSendSms"), driver2).click();
		 * 
		 * } else if (getElement(By.id("etPinCode"),driver2).isDisplayed()) {
		 * 
		 * getElement(By.id("etPinCode"), driver2).sendKeys("1111");
		 * 
		 * 
		 * }
		 */
		
		   
	   Thread.sleep(4000);

	 
		getElement(By.id("btnSendSms"), driver2).click();
		
		
		
		  
		  Thread.sleep(4000);
		
		getElement(By.id("etPinCode"), driver2).sendKeys("1111");
		  Thread.sleep(4000);

		
		  scrollDown(driver2);
		
		 Thread.sleep(2000);

		getElement(By.id("btnContinue"), driver2).click();
		
		Thread.sleep(3000);
		
		driver2.navigate().back();
		
		
		Thread.sleep(3000);
		
		driver2.navigate().back();
		
		
		
		
		
		
		
		
		
		
		
		
		
		


		getElement(By.id("homeBeansArrowIcon"), driver2).click();
		Thread.sleep(3000);
		
		
		
		/*
		 * driver2.executeScript("mobile: pressButton", ImmutableMap.of("command",
		 * "M"));
		 */
		 

				 

		
		getElement(By.id("aptAllActivities"), driver2).click();
		
		Thread.sleep(3000);
		
        
		driver2.navigate().back();

		
		Thread.sleep(3000);

		 scrollDown(driver2);
		 
		 Thread.sleep(3000);

		 
	        
		driver2.navigate().back();
		
		
		
		Thread.sleep(4000);
		
		
		 scrollDown(driver2);
		
		
		/*
		 * Thread.sleep(2000);
		 * 
		 * 
		 * getElement(By.id("tvHomeEarnAndRedeemPoints1"), driver2).click();
		 * Thread.sleep(3000);
		 * 
		 * 
		 * 
		 * getElement(By.id("tvRedeemBeans"), driver2).click(); Thread.sleep(3000);
		 * 
		 * 
		 * 
		 * 
		 * driver2.navigate().back(); Thread.sleep(3000);
		 * 
		 * 
		 * scrollDown2(driver2); Thread.sleep(3000);
		 * 
		 * scrollDown2(driver2); Thread.sleep(3000);
		 * 
		 * scrollDown2(driver2); Thread.sleep(2000);
		 * 
		 * 
		 * 
		 * 
		 * Thread.sleep(3000);
		 * 
		 * scrollDown2(driver2); Thread.sleep(3000);
		 */
		
		
		
		
		

		

	
		
		// Festive Range kısmında Check them out buttonu calısmıyor ve o yuzden burdan alttaki tablere yoneliyorum.
		
		
		 
		  
		
		/*
		 * getElement(By.id("item_coffee"), driver2).click(); Thread.sleep(4000);
		 * 
		 * getElement(By.id("rvMainCategory"), driver2).click(); Thread.sleep(4000);
		 * 
		 * getElement(By.id("rvSubCategory"), driver2).click(); Thread.sleep(4000);
		 * 
		 * getElement(By.id("ivProduct"), driver2).click(); Thread.sleep(5000);
		 * 
		 * driver2.navigate().back();
		 * 
		 * 
		 * getElement(By.id("rvSubCategory"), driver2).click();
		 */ 

		  
		  
		  Thread.sleep(3000);
		  
		  getElement(By.id("item_qr"), driver2).click(); 
		  Thread.sleep(3000);

		  
		  getElement(By.id("item_shops"), driver2).click(); 
		  Thread.sleep(2000);
		  
		  scrollDown2(driver2);
		  Thread.sleep(2000);
		  
		  scrollDown2(driver2);
		  Thread.sleep(2000);
		  
		  scrollDown2(driver2);
		  Thread.sleep(2000);
		  
		  getElement(By.id("etSearchStore"), driver2).click(); 
		  Thread.sleep(2000);
		  
		  getElement(By.id("etSearchStore"), driver2).sendKeys("Mall");
		  Thread.sleep(2000);
		  
		
		/*
		 * ((AndroidDriver)driver2).pressKeyCode(AndroidKeyCode.ENTER);
		 * 
		 * Thread.sleep(2000);
		 * 
		 * getElement(By.id("rvStores"), driver2).click();
		 * 
		 * Thread.sleep(2000);
		 * 
		 * scrollDown2(driver2);
		 * 
		 * Thread.sleep(2000);
		 * 
		 * driver2.navigate().back();
		 */
		  
		  
		 
		  getElement(By.id("item_profile"), driver2).click(); 
		  
          Thread.sleep(2000);
          

          
         
          
		
		  getElement(By.id("btnSignUpIn"), driver2).click(); Thread.sleep(2000);
		  
		  getElement(By.id("etName"), driver2).click();
		  getElement(By.id("etName"),driver2).clear();
		  
		  
			 KeyInput keyboard2 = new KeyInput("keyboard");
			 Sequence sendKeys2 = new Sequence(keyboard2, 0);
			 sendKeys2.addAction(keyboard2.createKeyDown("T".codePointAt(0)));
			 sendKeys2.addAction(keyboard2.createKeyUp("T".codePointAt(0)));

			 sendKeys2.addAction(keyboard2.createKeyDown("a".codePointAt(0)));
			 sendKeys2.addAction(keyboard2.createKeyUp("a".codePointAt(0)));
			 
			 sendKeys2.addAction(keyboard2.createKeyDown("s".codePointAt(0)));
			 sendKeys2.addAction(keyboard2.createKeyUp("s".codePointAt(0)));
			 
			 

			
			   driver2.perform(Arrays.asList(sendKeys2));
			   
			   Thread.sleep(4000);
			   
			   getElement(By.id("etSurname"), driver2).click();
			   getElement(By.id("etSurname"),driver2).clear();
			   
			   KeyInput keyboard3 = new KeyInput("keyboard");
			   Sequence sendKeys3 = new Sequence(keyboard3, 0);
				 sendKeys3.addAction(keyboard3.createKeyDown("D".codePointAt(0)));
				 sendKeys3.addAction(keyboard3.createKeyUp("D".codePointAt(0)));

				 sendKeys3.addAction(keyboard3.createKeyDown("o".codePointAt(0)));
				 sendKeys3.addAction(keyboard3.createKeyUp("o".codePointAt(0)));
				 
				 sendKeys3.addAction(keyboard3.createKeyDown("r".codePointAt(0)));
				 sendKeys3.addAction(keyboard3.createKeyUp("r".codePointAt(0)));
				
				 

				
				   driver2.perform(Arrays.asList(sendKeys3));
				   
				   Thread.sleep(4000);
				   
				   getElement(By.id("etEmail"), driver2).click();
				   getElement(By.id("etEmail"),driver2).clear();
				   Thread.sleep(3000);
				   
				   KeyInput keyboard4 = new KeyInput("keyboard");
				   Sequence sendKeys4 = new Sequence(keyboard4, 0);
					 sendKeys4.addAction(keyboard4.createKeyDown("d".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("d".codePointAt(0)));

					 sendKeys4.addAction(keyboard4.createKeyDown("o".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("o".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("r".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("r".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("u".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("u".codePointAt(0)));

					 sendKeys4.addAction(keyboard4.createKeyDown("k".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("k".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("v".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("v".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("u".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("u".codePointAt(0)));

					 sendKeys4.addAction(keyboard4.createKeyDown("@".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("@".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("g".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("g".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("m".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("m".codePointAt(0)));

					 sendKeys4.addAction(keyboard4.createKeyDown("a".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("a".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("i".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("i".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("l".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("l".codePointAt(0)));

					 sendKeys4.addAction(keyboard4.createKeyDown(".".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp(".".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("c".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("c".codePointAt(0)));
					 
					 sendKeys4.addAction(keyboard4.createKeyDown("o".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("o".codePointAt(0)));

					 sendKeys4.addAction(keyboard4.createKeyDown("m".codePointAt(0)));
					 sendKeys4.addAction(keyboard4.createKeyUp("m".codePointAt(0)));
					 
					 driver2.perform(Arrays.asList(sendKeys4));
					   
					 
					
					Thread.sleep(3000);
					
					  scrollDown2(driver2);
					  Thread.sleep(2000);
					  scrollDown2(driver2);
				   
				   
				  
					  getElement(By.id("cbAgreeToTerms"), driver2).click(); 
					  
					  Thread.sleep(3000);
						
					  
					  getElement(By.id("btnSignUp"), driver2).click(); 

					  Thread.sleep(3000);
					  
					  driver2.navigate().back();
					  Thread.sleep(3000);
					  driver2.navigate().back();
					  
					  
					



			   
					 
		 
		  
		  


  
		  scrollDown2(driver2);
		  Thread.sleep(2000);
		  
		  scrollDown2(driver2);
		  Thread.sleep(2000);
		  
		  getElement(By.id("tvProfileSeeAllMyOffers"), driver2).click(); 

		  Thread.sleep(2000);
		  
		  getElement(By.id("tvOfferHistory"), driver2).click(); 
		  
		  Thread.sleep(2000);

		  driver2.navigate().back();
		  
		  
		  
		  
		  Thread.sleep(2000);
		  
		  scrollUp(driver2);
		  Thread.sleep(2000);
		  scrollUp(driver2);
		  
		  Thread.sleep(2000);

		  
		  getElement(By.id("ivSettings"), driver2).click();
		  
		  Thread.sleep(2000);

		  getElement(By.id("rlContactPreferences"), driver2).click();
		  
		  Thread.sleep(2000);
		  
		  getElement(By.id("swMarketing"), driver2).click();
		  Thread.sleep(2000);
		  getElement(By.id("cbEmail"), driver2).click();
		  Thread.sleep(2000);
		  getElement(By.id("cbMobile"), driver2).click();
		  Thread.sleep(2000);
		  getElement(By.id("cbPush"), driver2).click();
		  Thread.sleep(2000);
		  getElement(By.id("btConfirm"), driver2).click();
		  Thread.sleep(2000);

		  getElement(By.id("rlFAQ"), driver2).click();
		  
		  Thread.sleep(2000);
		  
		  getElement(By.id("faqRecyclerView"), driver2).click();
		  Thread.sleep(2000);
		  
		  scrollDown(driver2);
		  
		  Thread.sleep(2000);

		  driver2.navigate().back();
		  
		  


		/*
		 * // To press some keys because the other way to press keys is deprecated
		 * 
		 * driver2.pressKey(new KeyEvent(AndroidKey.TAB));
		 */
		  
		  

		  
		  
		  

		  
		  
		  
		 

		  
		  

		  
		  

		  
		  
		  
		  


		  
		 
		
		
		
		
		



	
	
	

}
	
	
	
	public MobileElement getElement(By byelement, AndroidDriver<MobileElement> driver2) {

		WebDriverWait wait = new WebDriverWait(driver2, 58, 2000);
		MobileElement element = (MobileElement) wait.until(ExpectedConditions.presenceOfElementLocated(byelement));
		return element;

	}
	
	private static void scroll(AppiumDriver<?> driver2, int fromX, int fromY, int toX, int toY) {
		TouchAction touchAction = new TouchAction(driver2);
		touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();

	}

	public static void scroll(AppiumDriver<?> driver2, By by) {
		driver2.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		boolean isFoundTheElement = driver2.findElements(by).size() > 0;
		while (isFoundTheElement == false) {
			scrollDown(driver2);
			isFoundTheElement = driver2.findElements(by).size() > 0;
		}
		driver2.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		
	}

	public static void scrollDown(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
		int pressX = driver2.manage().window().getSize().width * 15 / 16;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver2.manage().window().getSize().height * 2 / 3;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver2.manage().window().getSize().height / 8;
		// scroll with TouchAction by itself
		scroll(driver2, pressX, bottomY, pressX, topY);
	}
	
	public static void scrollSide(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
		int pressX = driver2.manage().window().getSize().height / 2;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver2.manage().window().getSize().width * 8 / 10 ;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver2.manage().window().getSize().width * 2 / 10;
		// scroll with TouchAction by itself
		scroll(driver2, topY,pressX, bottomY, pressX);
	}
	
	public static void scrollDown2(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, bottomY, pressX, topY);
	}
	
	public static void scrollUp(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, topY, pressX, bottomY);
	}
	
	
	 
	
	
	
	
	
}

