package Costa_Mix.Costa_Mix;


import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.touch.offset.PointOption;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



import javax.imageio.ImageIO;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNotSame;
import static org.testng.Assert.assertSame;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Costa_No_Country_Selection {
	
	@Test
	public void CostaApp() throws InterruptedException, IOException, NotFoundException {
	
	

	final File appDir = new File("/Users/doruk/Library/Developer/Xcode/DerivedData/CostaLoyalty-dfoloyjoykdgzfcuhdkqahsvytqs/Build/Products/Debug-iphonesimulator");

	 final File app = new File(appDir, "CostaLoyalty.app"); // give the path of .app file/. ipa file .

	 DesiredCapabilities caps = new DesiredCapabilities();
	 
	   
	    
	 caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 11");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, 13.3);
		caps.setCapability(MobileCapabilityType.NO_RESET, true);
		caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
		caps.setCapability("useNewWDA", false);
		 caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
		
		 caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		 
		 caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("autoAcceptAlerts", true);
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("sensorInstrument", true);
			
			
		

		

		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");

		IOSDriver<MobileElement> driver = new IOSDriver<MobileElement>(url, caps);
		
		
		
	
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"), driver).click();
		Thread.sleep(1000);
		getElement(By.xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"), driver).clear();
		Thread.sleep(1000);
	
		driver.switchTo().activeElement().sendKeys("90000907");
		Thread.sleep(2000);
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Send SMS Code\"]"),driver).click();
		Thread.sleep(2000);

		driver.switchTo().activeElement().sendKeys("1111");
		Thread.sleep(2000);

		getElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]"),driver).click();
		Thread.sleep(2000);
		

		
		
		
		/*
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"MENU\"]"),driver).click(
		 * ); Thread.sleep(2000);
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"SCAN\"]"),driver).click(
		 * ); Thread.sleep(2000);
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"PROFILE\"]"),driver).
		 * click(); Thread.sleep(2000);
		 * 
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"HOME\"]"),driver).click(
		 * ); Thread.sleep(2000);
		 * 
		 * 
		 * scrollUp(driver);
		 * 
		 * 
		 * 
		 * 
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[4]"
		 * ),driver).click(); Thread.sleep(2000);
		 * 
		 * 
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton"
		 * ),driver).click(); Thread.sleep(3000);
		 * 
		 * 
		 * 
		 * 
		 * getElement(By.id("icon arrowLeftWhite"), driver).click();
		 * 
		 * 
		 * Thread.sleep(2000);
		 * 
		 * getElement(By.id("icon crossWhite"),driver).click(); Thread.sleep(3000);
		 * 
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[3]"
		 * ),driver).click(); Thread.sleep(3000);
		 * 
		 * getElement(By.id("icon arrowLeftWhite"), driver).click(); Thread.sleep(2000);
		 * 
		 * 
		 * 
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[2]"
		 * ),driver).click(); Thread.sleep(3000);
		 * 
		 * getElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Redeem Beans\"]"),
		 * driver).click(); Thread.sleep(3000);
		 * 
		 * 
		 * getElement(By.id("icon crossBlack"),driver).click(); Thread.sleep(3000);
		 * 
		 * scrollDown2(driver);
		 */
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"SCAN\"]"), driver).click();
		
		Thread.sleep(3000);
		
		
		
		  String folder_name4 = "screenpoot"; SimpleDateFormat dfqr4 = new
		  SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa"); new File(folder_name4).mkdir();
		String file_name4 = dfqr4.format(new Date())
				+ ".png";
		
		/*
		 * File fqr2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 * FileUtils.copyFile(fqr2, new File(folder_name2 + "/" + file_name2));
		 */
		 
		 
		 
		
		  MobileElement element4 = driver.findElement(By.
		  xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeImage[1]"
		  )); File fqr4 =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  Point point4 = element4.getLocation(); 
		  int xcordinate = point4.getX(); 
		  int ycordinate = point4.getY(); 
		  int imageWidth = element4.getSize().getWidth();
		  //Retrieve height of element. 
		  int imageHeight =
		  element4.getSize().getHeight();
		  
		  BufferedImage imgqr4 = ImageIO.read(fqr4);
		  
		  //cut Image using height, width and x y coordinates parameters. 
		  
		  BufferedImage destinationqrqr4 = imgqr4.getSubimage(20, 490, 590, 590);
		  ImageIO.write(destinationqrqr4, "png", fqr4);
		  
		  //save Image screenshot In D: drive.
		  
		  FileUtils.copyFile(fqr4, new File(folder_name4 + "/" + file_name4));
		 
		  String qrCodeno4 = decodeQRCode(destinationqrqr4);
		  System.out.println("QrCode is succesfully scanned: "+qrCodeno4);
		  
		  
		  
		  
		  String qrCodes4= qrCodeno4 ;
	        String[] arrOfStr = qrCodes4.split("A", 5); 
	 
	        for (String a : arrOfStr) {
	        	
	        	
	        	if(a.length()>0) {
	        		
	        		
	        		System.out.println("QrCode is correct and is "+a+" and has the following number of characters: "+a.length());
	        	}
	        
	        }
	        
	       assertNotNull(qrCodeno4);
	       
	       
	      long qrProfileNumber4 = 9990000000009461L;
	       String qrControle4=arrOfStr[2];
	       
	       Assert.assertEquals(qrControle4,String.valueOf(qrProfileNumber4));
	       
	       
		  
		  
		  
		  getElement(By.xpath("//XCUIElementTypeButton[@name=\"HOME\"]"), driver).click();
			 
			 Thread.sleep(3000);
			 
			 scrollDown2(driver);
			 Thread.sleep(3000);
			 scrollDown2(driver);
			 Thread.sleep(3000);
			 scrollUp(driver);
			 Thread.sleep(3000);
			 
				

				getElement(By.xpath("(//XCUIElementTypeButton[@name=\"Check them out\"])[1]"),driver).click();
				Thread.sleep(2000);
				
				
				
				Thread.sleep(3000);
				scrollDown2(driver);
				Thread.sleep(3000);
				
				scrollUp(driver);
				Thread.sleep(3000);
				
				getElement(By.id("icon arrowLeftWhite"), driver).click();
				Thread.sleep(3000);
				getElement(By.xpath("(//XCUIElementTypeButton[@name=\"Check them out\"])[1]"),driver).click();
				Thread.sleep(2000);
				
				
				
				Thread.sleep(3000);
				scrollDown2(driver);
				Thread.sleep(3000);
				
				scrollUp(driver);
				Thread.sleep(3000);
				
				getElement(By.id("icon arrowLeftWhite"), driver).click();
				Thread.sleep(3000);
				
				
				 getElement(By.xpath("//XCUIElementTypeButton[@name=\"SCAN\"]"), driver).click();
Thread.sleep(3000);
getElement(By.xpath("//XCUIElementTypeButton[@name=\"HOME\"]"), driver).click();
Thread.sleep(3000);
				 getElement(By.xpath("//XCUIElementTypeButton[@name=\"SCAN\"]"), driver).click();
				 Thread.sleep(3000);
				 
				 
				 
		  
				 String folder_name5 = "screenshotta"; SimpleDateFormat
				 dfqr5 = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
				 new File(folder_name5).mkdir();
				 String file_name5 = dfqr5.format(new Date()) + ".png";
				
				
				 
				  File fqr5 =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				  
				  BufferedImage imgqr5 = ImageIO.read(fqr5);
				  
				  //cut Image using height, width and x y coordinates parameters. 
				  
				  BufferedImage destinationqrqr5 = imgqr5.getSubimage(20, 490, 590, 590);
				  ImageIO.write(destinationqrqr5, "png", fqr5);
				  
				  //save Image screenshot In D: drive.
				  
				  
				  
				  String qrCode1no5 = decodeQRCode(destinationqrqr5);
				  System.out.println();
				  System.out.println("QrCode is succesfully scanned: "+qrCode1no5);
		
				  if(qrCodeno4.equals(qrCode1no5)) { 
					  System.out.println("qrCode has not refreshed");
					  
				  }
				  
				  else {
					  System.out.println("qrCode has succesfully refreshed, new qrCode is not the same as the old one. ");
					  
					  
				  }
				  
				  
				  assertNotSame(qrCodeno4,qrCode1no5);
				  
				  
				  getElement(By.xpath("//XCUIElementTypeButton[@name=\"HOME\"]"), driver).click();
					 
					 Thread.sleep(3000);
					 
					 scrollDown(driver);
                     Thread.sleep(3000);
					 
					 scrollDown(driver);
					 
					 
					 Thread.sleep(3000);
					 
					 
		
		  getElement(By.
		  xpath("//XCUIElementTypeStaticText[@name=\"Discover the offers\"]"),driver).
		  click();
		  
		  Thread.sleep(2000);
		  
		  driver.shake();
		  
		  Thread.sleep(2000);
		  
			  
		  
		  
		/*
		 * getElement(By.xpath("(//XCUIElementTypeButton[@name=\"Choose\"])[1]"),driver)
		 * .click();
		 * 
		 * Thread.sleep(2000);
		 */
		  
		  getElement(By.
		  xpath("//XCUIElementTypeStaticText[@name=\"See all my offers\"]"),driver).
		  click(); Thread.sleep(2000);
		 
				  
				getElement(By.xpath("//XCUIElementTypeButton[@name=\"Use\"]"),driver).click();
				
				Thread.sleep(3000);
				
				String folder_name6 = "screenshoooot"; SimpleDateFormat dfqr6 = new
						  SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa"); new File(folder_name6).mkdir();
						String file_name6 = dfqr6.format(new Date())
								+ ".png";
				
				 File f6 =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
				  
				  BufferedImage imgqr6 = ImageIO.read(f6);
				  
				  //cut Image using height, width and x y coordinates parameters. 
				  
				  BufferedImage destinationqrqr6 = imgqr6.getSubimage(20, 490, 590, 590);
				  ImageIO.write(destinationqrqr6, "png", f6);
				  
				  //save Image screenshot In D: drive.
				  
				  
				  
				  String qrCode2no6 = decodeQRCode(destinationqrqr6);
				  
				  System.out.println("Couponcode is succesfully scanned: "+qrCode2no6);
				
				
				  String qrCodeUse= qrCode2no6 ;
			        String[] arrOfStrUse = qrCodeUse.split("A", 5); 
			  
			        for (String b : arrOfStrUse) {
			        	System.out.println(b);
			        	
			        	if(b.length()>0) {
			        		
			        		
			        		System.out.println("QrCode is correct and is "+b+" and has the following number of characters: "+b.length());
			        	}
			        
			        }
				  
			        
			        assertNotSame(qrCodeno4,qrCode2no6);
			        assertNotSame(qrCode1no5,qrCode2no6);
				  

				//XCUIElementTypeApplication[@name="Costa Coffee Club"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeImage[1]

				  
		/*
		 * generateImage(element, fqr2); readQRCode(); decodeQRCode(destinationqrqr);
		 */
		  
		  
		
		  
		  
		
		
		/*
		 * MobileElement qrCodeElement =
		 * driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"SCAN\"]")); File
		 * screenshot = driver.getScreenshotAs(OutputType.FILE);
		 * 
		 * String content = decodeQRCode(generateImage(qrCodeElement, screenshot));
		 * System.out.println("content = " + content);
		 */
		
		
		
		Thread.sleep(6000);
		
		getElement(By.xpath("//XCUIElementTypeButton[@name=\"HOME\"]"), driver).click();
		
		Thread.sleep(2000);
		
		scrollDown2(driver);
		
		scrollDown2(driver);
		
		
		
		
	
		
		
	
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		/*
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[4]"
		 * ),driver).click(); Thread.sleep(2000);
		 * 
		 * 
		 * getElement(By.
		 * xpath("//XCUIElementTypeApplication[@name=\"Costa Coffee Club\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton"
		 * ),driver).click(); Thread.sleep(2000);
		 * 
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"icon arrowLeftWhite\"]")
		 * ,driver).click();
		 * 
		 * Thread.sleep(2000);
		 * 
		 * getElement(By.xpath("//XCUIElementTypeButton[@name=\"icon close\"]"),
		 * driver).click();
		 */
		
		
		
		
		
		/*
		 * getElement(By.
		 * xpath("//XCUIElementTypeStaticText[@name=\"Discover the offers\"]"),driver).
		 * click();
		 * 
		 * Thread.sleep(2000);
		 * 
		 * driver.shake();
		 * 
		 * Thread.sleep(2000);
		 * 
		 * getElement(By.xpath("(//XCUIElementTypeButton[@name=\"Choose\"])[3]"),driver)
		 * .click();
		 * 
		 * Thread.sleep(2000);
		 */
		 
		 




		
		
		

		
		
		
		
		
		
		
	 

		/*
		 * caps.setCapability("appPackage", "package of your application"); //package of
		 * the application
		 * 
		 * caps.setCapability("appActivity", "activity of your application"); //app
		 * activity of the application
		 */	 

}
	

	
	
	
	public MobileElement getElement(By byelement, IOSDriver<MobileElement> driver2) {

		WebDriverWait wait = new WebDriverWait(driver2, 58, 2000);
		MobileElement element = (MobileElement) wait.until(ExpectedConditions.presenceOfElementLocated(byelement));
		return element;

	}
	
	 
	
	
	public MobileElement getElement2 (By byelement, IOSDriver<MobileElement>driver3) {
	
	             
       MobileElement element=driver3.findElement(By.xpath("//XCUIElementTypeButton[@name=\\\"icon arrowLeftWhite\\\"]"));
       TouchActions actions=new TouchActions(driver3);
       actions.singleTap(element).perform();          

      return element;  
	}
  
  
  
	private static void scroll(AppiumDriver<?> driver, int fromX, int fromY, int toX, int toY) {
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();

	}
	

	public static void scroll(AppiumDriver<?> driver, By by) {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		boolean isFoundTheElement = driver.findElements(by).size() > 0;
		while (isFoundTheElement == false) {
			scrollDown(driver);
			isFoundTheElement = driver.findElements(by).size() > 0;
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		
	}

	public static void scrollDown(AppiumDriver<?> driver) {
		// if pressX was zero it didn't work for me
		int pressX = driver.manage().window().getSize().width * 15 / 16;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver.manage().window().getSize().height * 2 / 3;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver.manage().window().getSize().height / 8;
		// scroll with TouchAction by itself
		scroll(driver, pressX, bottomY, pressX, topY);
	}
	
	public static void scrollUp(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, topY, pressX, bottomY);
	}
	
	
	
	public static void scrollDown2(AppiumDriver<?> driver2) {
		// if pressX was zero it didn't work for me
				int pressX = driver2.manage().window().getSize().width * 8 / 16;
				// 4/5 of the screen as the bottom finger-press point
				int bottomY = driver2.manage().window().getSize().height * 6 / 10;
				// just non zero point, as it didn't scroll to zero normally
				int topY = driver2.manage().window().getSize().height * 3 / 10;
				// scroll with TouchAction by itself
				scroll(driver2, pressX, bottomY, pressX, topY);
	}
	
	
	


    private AppiumDriver<MobileElement> driver;
    private AppiumDriverLocalService service;

  

    /**
     * This test capture the screenshot and get the element that contains the QRCode
     * Based on the element points (width and height) the image os cropped
     * With cropped image we can decode the QRCode with zxing
     * @throws InterruptedException 
     */
   

    /**
     * Return a cropped image based on an element (in this case the qrcode image) from the entire device screenshot
     * @param element elemement that will show in the screenshot
     * @param screenshot the entire device screenshot
     * @return a new image in BufferedImage object
     * @throws IOException if any problem in generate image occurs
     */
    private static BufferedImage generateImage( MobileElement element, File screenshot) throws IOException {
        BufferedImage fullImage = ImageIO.read(screenshot);
        Point imageLocation = element.getLocation();

        int qrCodeImageWidth = element.getSize().getWidth();
        int qrCodeImageHeight = element.getSize().getHeight();

        int pointXPosition = imageLocation.getX();
        int pointYPosition = imageLocation.getY();

        BufferedImage qrCodeImage = fullImage.getSubimage(pointXPosition, pointYPosition, qrCodeImageWidth, qrCodeImageHeight);
        ImageIO.write(qrCodeImage, "png", screenshot);

        return qrCodeImage;
    }

    /**
     * Decode a QR Code image using zxing
     * @param qrCodeImage the qrcode image cropped from entire device screenshot
     * @return the content
     * @throws NotFoundException if the image was not found
     */
    private static String decodeQRCode(BufferedImage qrCodeImage) throws NotFoundException {
        LuminanceSource source = new BufferedImageLuminanceSource(qrCodeImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Result result = new MultiFormatReader().decode(bitmap);
        return result.getText();
    }
    
  

}